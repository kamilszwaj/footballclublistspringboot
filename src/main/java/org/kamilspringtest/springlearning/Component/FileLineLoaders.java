package org.kamilspringtest.springlearning.Component;


import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


@Component
public class FileLineLoaders {
    public List<String> loadLines(String fileName){
        try {
            return Files.readAllLines(
                    Paths.get(new ClassPathResource(fileName).getURI())
            );
        }catch (IOException e){
            throw new RuntimeException(String.format("Error during reading lines from file %s " +fileName), e);
        }
    }
}
