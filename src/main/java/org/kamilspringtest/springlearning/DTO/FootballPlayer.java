package org.kamilspringtest.springlearning.DTO;

import java.math.BigDecimal;
import java.util.Objects;

public class FootballPlayer {
    private String surname;
    private Integer number;
    private String position;
    private BigDecimal value;
    private String clubName;

    public FootballPlayer( String surname, Integer number, String position, BigDecimal value, String clubName) {
        this.surname = surname;
        this.number = number;
        this.position = position;
        this.value = value;
        this.clubName = clubName;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getNumber() {
        return number;
    }

    public String getPosition() {
        return position;
    }

    public BigDecimal getValue(){
        return this.value;
    }

    public String getClubName() {
        return clubName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FootballPlayer)) return false;
        FootballPlayer that = (FootballPlayer) o;
        return Objects.equals(surname, that.surname) &&
                Objects.equals(number, that.number) &&
                Objects.equals(position, that.position) &&
                Objects.equals(value, that.value) &&
                Objects.equals(clubName, that.clubName);
    }

    @Override
    public int hashCode() {
        int hashcodeResult = number != null ? number.hashCode() : 0;
        hashcodeResult = 31 * hashcodeResult + (this.clubName != null ? this.clubName.hashCode() :0);
        hashcodeResult = 31 * hashcodeResult + (this.position != null ? this.position.hashCode() : 0);
        hashcodeResult = 31 * hashcodeResult + (this.surname != null ? this.surname.hashCode() : 0);
        hashcodeResult = 31 * hashcodeResult + (this.value != null ? this.value.hashCode() : 0);
        return hashcodeResult;
    }
}
