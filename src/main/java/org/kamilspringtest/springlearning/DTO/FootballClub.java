package org.kamilspringtest.springlearning.DTO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class FootballClub {
    private String clubName;
    private Date clubDateCreation;
    private List<FootballPlayer>  clubPlayers;
    private BigDecimal clubValue;

    public FootballClub(String clubName, Date clubDateCreation, List<FootballPlayer> clubPlayers){
        this.clubValue=BigDecimal.ZERO;
        this.clubName = clubName;
        this.clubDateCreation = clubDateCreation;
        this.clubPlayers = clubPlayers;
        for (FootballPlayer Player:this.clubPlayers
             ) {
            this.clubValue=this.clubValue.add(Player.getValue());
        }
    }

    public String getClubName() {
        return clubName;
    }

    public Date getClubDateCreation(){
        return this.clubDateCreation;
    }

    public BigDecimal getClubValue() {
        return this.clubValue;
    }

    public List<FootballPlayer> getClubPlayers(){
        return this.clubPlayers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FootballClub)) return false;
        FootballClub that = (FootballClub) o;
        return clubName.equals(that.clubName) &&
                clubDateCreation.equals(that.clubDateCreation) &&
                clubPlayers.equals(that.clubPlayers) &&
                clubValue.equals(that.clubValue);
    }

    @Override
    public int hashCode() {
        int hashcodeResult = clubName!=null ? clubName.hashCode():0;
        hashcodeResult=31*hashcodeResult+(clubDateCreation!=null ? clubDateCreation.hashCode():0);
        hashcodeResult=31*hashcodeResult+(clubValue != null ? clubValue.hashCode():0);
        hashcodeResult=31*hashcodeResult+(clubPlayers != null ? clubPlayers.hashCode():0);
        return hashcodeResult;
    }

    @Override
    public String toString() {
        return "FootballClub{" +
                "clubName='" + clubName + '\'' +
                ", clubDateCreation=" + clubDateCreation +
                ", clubPlayers=" + clubPlayers +
                ", clubValue=" + clubValue +
                '}';
    }
}
