package org.kamilspringtest.springlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringlearningApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringlearningApplication.class, args);
	}

}

