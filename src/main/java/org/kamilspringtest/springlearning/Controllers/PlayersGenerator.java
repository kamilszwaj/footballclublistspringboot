package org.kamilspringtest.springlearning.Controllers;

import org.kamilspringtest.springlearning.Component.FileLineLoaders;
import org.kamilspringtest.springlearning.DTO.FootballPlayer;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PlayersGenerator {
    private static final String FILE_NAME_PLAYERS = "Players";
    private final FileLineLoaders filesLineLoader;
    private Integer numberOfPlayers;

    public PlayersGenerator(FileLineLoaders filesLineLoader){
        this.filesLineLoader = filesLineLoader;
        numberOfPlayers=0;
    }

    List<FootballPlayer> create(){
        return filesLineLoader.loadLines(FILE_NAME_PLAYERS).stream()
                .map(s -> s.split(","))
                .map(strings -> new FootballPlayer(strings[1].replaceFirst("\\s",""),
                        new Integer(strings[0]),strings[2].replaceFirst("\\s",""),new BigDecimal(new Float(strings[3].replaceFirst("\\s",""))),strings[4].replaceFirst("\\s","")))
                .collect(Collectors.toList());

    }

    private String getSurname(String[] lineFromFile){
        return lineFromFile[1];
    }

    private Integer getNumber(String[] lineFromFile){
        return new Integer(lineFromFile[0]);
    }

    private String getPosition(String[] lineFromFile){
        return lineFromFile[2];
    }

    private BigDecimal getValue(String[] lineFromFile){
        return new BigDecimal(lineFromFile[3].replaceFirst("\\s",""));
    }

    private String getClubName(String[] lineFromFile){
        return lineFromFile[4].replaceFirst("\\s","");
    }

}
