package org.kamilspringtest.springlearning.Controllers;



import org.kamilspringtest.springlearning.DTO.FootballClub;
import org.kamilspringtest.springlearning.DTO.FootballPlayer;
import org.kamilspringtest.springlearning.Repository.FootballClubsReposityory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/FootballClubs")
@RestController
public class FootballClubController {

    private final FootballClubsReposityory FootballClubsRepository;
    private final PlayersGenerator playersGenerator;

    public FootballClubController(FootballClubsReposityory footballClubsRepository, PlayersGenerator playersGenerator) {
        this.FootballClubsRepository = footballClubsRepository;
        this.playersGenerator = playersGenerator;
    }

    @GetMapping
    List<FootballClub> findAll() {
        return FootballClubsRepository.findAll();
    }

    @GetMapping("/{footballClubName}")
    FootballClub findOne(@PathVariable("footballClubName") String footballClubname) {
        return FootballClubsRepository.findOne(footballClubname);
    }

    @GetMapping("/{footballClubName}/allPlayers")
    List<FootballPlayer> findAllPlayers(@PathVariable("footballClubName") String footballClubName) {
        return FootballClubsRepository.findOne(footballClubName).getClubPlayers();

    }

    @GetMapping("/footballPlayers")
    List<FootballPlayer> allPlayers() {
        return FootballClubsRepository.findAll().stream()
                .map(footballClub -> footballClub.getClubPlayers())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }



    @GetMapping("/footballPlayers/{playersPosition:[a-Az-Z]+}")
    List<FootballPlayer> findPlayersFromPosition(@PathVariable("playersPosition") String playersPosition) {
        List<FootballPlayer> listOfPlayers = new LinkedList<FootballPlayer>();
        for (FootballClub footbalClub : FootballClubsRepository.findAll()
        ) {
            for (FootballPlayer footballPlayer : footbalClub.getClubPlayers()
            ) {
                if (footballPlayer.getPosition().equals(playersPosition))
                    listOfPlayers.add(footballPlayer);
            }

        }

        return listOfPlayers;
    }


}
