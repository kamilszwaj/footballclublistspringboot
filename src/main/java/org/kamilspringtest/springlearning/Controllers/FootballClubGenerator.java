package org.kamilspringtest.springlearning.Controllers;

import org.joda.time.LocalDate;
import org.kamilspringtest.springlearning.Component.FileLineLoaders;
import org.kamilspringtest.springlearning.DTO.FootballClub;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FootballClubGenerator {
    private final PlayersGenerator playersGenerator;
    private final FileLineLoaders fileLineLoaders;

    FootballClubGenerator(PlayersGenerator playersGenerator,FileLineLoaders fileLineLoaders){
        this.playersGenerator = playersGenerator;
        this.fileLineLoaders  = fileLineLoaders;
    }

    public List<FootballClub> generateClubs(){

        /*Streams to generate List of FootbalPlayer, who is playing in this FootballClub
        * Filter is providing the confident result of players to suitable FootbalClub*/

        return fileLineLoaders.loadLines("Clubs")
                .stream()
                .map(s -> s.split(","))
                .map(strings -> new FootballClub(strings[0],(new LocalDate(new Integer(strings[1].replaceFirst(" ","")),1,1).toDate()),
                        playersGenerator
                        .create()
                        .stream()
                        .filter(footballPlayer -> footballPlayer.getClubName().equals(strings[0]))
                        .collect(Collectors.toList())
                        )
                ).collect(Collectors.toList());
      }


}
