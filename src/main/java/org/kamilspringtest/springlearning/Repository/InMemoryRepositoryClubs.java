package org.kamilspringtest.springlearning.Repository;

import org.kamilspringtest.springlearning.Controllers.FootballClubGenerator;
import org.kamilspringtest.springlearning.DTO.FootballClub;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Repository
public class InMemoryRepositoryClubs implements FootballClubsReposityory{

    /*String is the name of club, FootballClub is the class of FootballClub*/
    private Map<String, FootballClub> FootballClubs;

    public InMemoryRepositoryClubs(FootballClubGenerator footballClubGenerator){
        FootballClubs = footballClubGenerator.generateClubs()
                .stream()
                .collect(Collectors.toMap(FootballClub::getClubName, Function.identity()));
    }

    @Override
    public List<FootballClub> findAll() {
        return new LinkedList<>(FootballClubs.values());
    }

    @Override
    public FootballClub findOne(String name) {
        return FootballClubs.get(name);
    }

    @Override
    public FootballClub save(FootballClub footballClub) {
        return null;
    }
}
