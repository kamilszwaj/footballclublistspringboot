package org.kamilspringtest.springlearning.Repository;

import org.kamilspringtest.springlearning.DTO.FootballClub;

import java.util.List;

public interface FootballClubsReposityory {
    List<FootballClub> findAll();
    FootballClub findOne(String name);
    FootballClub save(FootballClub footballClub);
}
